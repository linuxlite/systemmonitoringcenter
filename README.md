System Monitoring Center
=====================

Fork of: https://github.com/hakandundar34coding/system-monitoring-center

## Screenshot:

![](https://imgur.com/nWXnbv8.png)

## License ![License](https://img.shields.io/badge/license-GPLv3-green.svg)

This project is under the GPLv3 license. Unless otherwise stated in individual files.

## Authors
- [Jerry Bezencon](https://gitlab.com/linuxlite/)
- [Hakan Dündar](https://github.com/hakandundar34coding)
